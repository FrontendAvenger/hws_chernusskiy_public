/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

let Train = {
	name: "Intercity",
	speed: 200,
	pass_amount: 1000,
	Go: function (){
		console.log("Поезд ",this.name," везет ",this.pass_amount ," пассажиров со скоростью ",this.speed);
	},
	Stop: function(){
		this.speed = 0;
		console.log("Поезд", this.name, "остановился. Скорость", this.speed);
	},
	TakePass: function(){
		let x = 100;
		this.pass_amount += x;
		console.log(this.pass_amount);
	}

}
	Train.Go();
	Train.Stop();
	Train.TakePass();
	Train.TakePass();
	Train.TakePass();